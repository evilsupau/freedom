// overload.cpp : 定義主控台應用程式的進入點。
//

#include "stdafx.h"

static unsigned int arU32Reg[1024];

class NFC_REG {
private:
    unsigned int u32Addr;

public:
    // constructor
    NFC_REG(unsigned int u32Addr)
    {
        this->u32Addr = u32Addr;
    }

    // covert object to int type
    operator unsigned int() const {
        return arU32Reg[u32Addr];
    }

    // assignment overload
    NFC_REG &operator = (unsigned int u32Val) {
        arU32Reg[u32Addr] = u32Val;

        return *this;
    }
};

NFC_REG REG32(unsigned int u32RegAddr)
{
    NFC_REG reg(u32RegAddr);

    return reg;
}

int main()
{
    // initial the register value
    for (int i = 0; i < 1024; i++)
    {
        arU32Reg[i] = i;
    }

    unsigned int a = REG32(0x100);  // a = 256

    REG32(0x100) = 0x200;   // set arU32Reg[0x100] to 512

    a = REG32(0x100);   // a = 512

    return 0;
}

